<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
    }

    public function list(Request $request)
    {
        $perPage = $request->get('perPage', 10);
        $sortField = $request->get('sortField', 'name');
        $sortOrder = $request->get('sortOrder', 'asc');
        $arrayName = $request->get('name', []);
        $arrayCodigo = $request->get('num_ref', []);
        $models = Product::when($arrayName, function ($query, $arrayName) {
            $query->where(function ($q) use ($arrayName) {
                foreach ($arrayName as $name) {
                    $q->orWhere('name', 'like', "%{$name}%");
                }
            });

            return $query;
        })->when($arrayCodigo, function ($query, $arrayCodigo) {
            $query->where(function ($q) use ($arrayCodigo) {
                foreach ($arrayCodigo as $name) {
                    $q->orWhere('num_ref', 'like', "%{$name}%");
                }
            });

            return $query;
        })->orderBy($sortField, $sortOrder)->paginate($perPage);

        return $models;
    }

    public function store(Request $request)
    {
        $validator = validator($request->all(), [
            'num_ref' => 'required|unique:products,num_ref',
            'name' => 'required',
            'quantity' => 'required|numeric|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $product = new Product();
        $product->fill($request->all());
        $product->save();

        return response()->json($product, 201);
    }

    public function update(Request $request)
    {
        $validator = validator($request->all(), [
            'num_ref' => 'required|unique:products,num_ref,'.$request->id,
            'name' => 'required',
            'quantity' => 'required|numeric|min:1',
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors(), 'success' => false], 422);
        }
        $product = Product::find($request->id);
        $product->fill($request->all());
        $product->save();

        return response()->json($product, 201);
    }
}
